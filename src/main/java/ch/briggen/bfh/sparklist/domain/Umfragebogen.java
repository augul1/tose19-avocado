package ch.briggen.bfh.sparklist.domain;

/**
 * Einzelner Eintrag in der Liste mit Name, eid, pid  (und einer eindeutigen uid)
 * @author TOSE Avocado 2019
 *
 */

public class Umfragebogen {
	private long uid;
	private String name;
	private long eid;
	private long pid;
	
	public Umfragebogen(long uid, String name, long eid, long pid) {
		super();
		this.uid = uid;
		this.name = name;
		this.eid = eid;
		this.pid = pid;
	}
	public long getEid() {
		return eid;
	}
	public void setEid(long eid) {
		this.eid = eid;
	}
	public long getPid() {
		return pid;
	}
	public void setPid(long pid) {
		this.pid = pid;
	}
	public long getUid() {
		return uid;
	}
	public void setUid(long uid) {
		this.uid = uid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}

	public Umfragebogen() {
		
	}
	@Override
	public String toString() {
		return "Umfragebogen [uid=" + uid + ", name=" + name + ", eid=" + eid + ", pid=" + pid + "]";
	}
	
	
	
	

}
