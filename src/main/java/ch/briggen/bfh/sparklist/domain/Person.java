package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Vorname (und einer eindeutigen pid)
 * @author TOSE Avocado 2019
 *
 */
public class Person {
	
	private long pid;
	private String name;
	private String vorname;
	

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Person()
	{
		
	}


	public long getPid() {
		return pid;
	}


	public void setPid(long pid) {
		this.pid = pid;
	}


	public String getName() {
		return name;
	}


	public void setName(String name) {
		this.name = name;
	}


	public String getVorname() {
		return vorname;
	}


	public void setVorname(String vorname) {
		this.vorname = vorname;
	}


	@Override
	public String toString() {
		return "Person [pid=" + pid + ", name=" + name + ", vorname=" + vorname + "]";
	}


	public Person(long pid, String name, String vorname) {
		super();
		this.pid = pid;
		this.name = name;
		this.vorname = vorname;
	}
	

}
