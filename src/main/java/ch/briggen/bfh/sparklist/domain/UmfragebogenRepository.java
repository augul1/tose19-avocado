package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Umfragebogen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Umfragebogen implementiert
 * @author TOSE Avocado 2019
 *
 */


public class UmfragebogenRepository {
	
	private final Logger log = LoggerFactory.getLogger(FrageRepository.class);
	

	/**
	 * Liefert alle umfragebogen in der Datenbank
	 * @return Collection aller Umfragebogen
	 */
	public Collection<Umfragebogen> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select uid, name, eid, pid from umfragebogen");
			ResultSet rs = stmt.executeQuery();
			return mapUmfragebogen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all umfragebogen. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}



	/**
	 * Liefert das Umfragebogen mit der übergebenen uid
	 * @param uid uid des Umfragebogen
	 * @return Umfragebogen oder NULL
	 */
	public Umfragebogen getByUid(long uid) {
		log.trace("getByUid " + uid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select uid, name, eid, pid from umfragebogen where uid=?");
			stmt.setLong(1, uid);
			ResultSet rs = stmt.executeQuery();
			return mapUmfragebogen(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by uid " + uid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	
	/**
	 * Speichert das übergebene Umfragebogen in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Umfragebogen i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update umfragebogen set name=?, eid=?, pid=? where uid=?");
			stmt.setString(1, i.getName());
			stmt.setLong(2, i.getEid());
			stmt.setLong(3, i.getPid());
			stmt.setLong(4, i.getUid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating fragen " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Umfragebogen mit der angegebenen Id von der DB
	 * @param uid Umfragebogen ID
	 */
	public void delete(long uid) {
		log.trace("delete " + uid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from umfragebogen where uid=?");
			stmt.setLong(1, uid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing fragen by uid " + uid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}		
	}

	/**
	 * Speichert das angegebene Umfragebogen in der DB. INSERT.
	 * @param i neu zu erstellendes Umfragebogen
	 * @return Liefert die von der DB generierte uid des neuen Umfragebogen zurück
	 */
	public long insert(Umfragebogen i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into umfragebogen (name,eid,pid) values (?,?,?)");
			stmt.setString(1, i.getName());
			stmt.setLong(2, i.getEid());
			stmt.setLong(3, i.getPid());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long uid = key.getLong(1);
			return uid;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating umfragebogen " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Umfragebogen-Objekte. Siehe getByXXX Methoden.
	 * @author TOSE Avocado 2019
	 * @throws SQLException 
	 *
	 */
	private static Collection<Umfragebogen> mapUmfragebogen(ResultSet rs) throws SQLException 
	{
		LinkedList<Umfragebogen> list = new LinkedList<Umfragebogen>();
		while(rs.next())
		{
			Umfragebogen i = new Umfragebogen(rs.getLong("uid"),rs.getString("name"),rs.getLong("eid"),rs.getLong("pid"));
			list.add(i);
		}
		return list;
	}

}
