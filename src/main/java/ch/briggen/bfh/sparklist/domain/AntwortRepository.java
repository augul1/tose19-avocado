package ch.briggen.bfh.sparklist.domain;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import java.sql.SQLException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortFrageUmfragebogen;


/**
 * Repository für alle Antworten. 
 * Hier werden alle Funktionen für die DB-Operationen zu Antworten implementiert
 * @author TOSE Avocado
 *
 */


public class AntwortRepository {
	
	String tablevar;
	
	private final Logger log = LoggerFactory.getLogger(AntwortRepository.class);
	

	/**
	 * Liefert alle antworten in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Antwort> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, antwort, fid from antworten");
			ResultSet rs = stmt.executeQuery();
			return mapAntworten(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all antworten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	

	/**
	 * Liefert das Antwort mit der übergebenen Id
	 * @param id id des Antwort
	 * @return Antwort oder NULL
	 */
	public Antwort getById(long id) {
		log.trace("getById " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, antwort, fid from antworten where id=?");
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			return mapAntworten(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	public Collection<Antwort> getByFid(long fid) {
		log.trace("getById " + fid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select id, antwort, fid from antworten where fid=?");
			stmt.setLong(1, fid);
			ResultSet rs = stmt.executeQuery();
			return mapAntworten(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by fid " + fid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}


	/**
	 * Speichert das übergebene Antwort in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Antwort i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update antworten set antwort=?, fid=? where id=?");
			stmt.setString(1, i.getAntwort());
			stmt.setLong(2, i.getFid());
			stmt.setLong(3, i.getId());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating antwort " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Antwort mit der angegebenen Id von der DB
	 * @param id Antwort ID
	 */
	public void delete(long id) {
		log.trace("delete " + id);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from antworten where id=?");
			stmt.setLong(1, id);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing antworten by id " + id;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Speichert das angegebene Antwort in der DB. INSERT.
	 * @param i neu zu erstellendes Antwort
	 * @return Liefert die von der DB generierte id des neuen Antwort zurück
	 */
	public long insert(Antwort i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into antworten (antwort, fid) values (?,?)");
			stmt.setString(1, i.getAntwort());
			stmt.setLong(2, i.getFid());
	
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long id = key.getLong(1);
			return id;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating item " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Liefert das Ergebniss INNER JOIN DER VIEW ANTWORTFRAGEUMFRAGE als Java Objekt zurück
	 * @return Collection aller AntwortFrageUmfragebogen
	 */
	public Collection<AntwortFrageUmfragebogen> getAllData()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT id, antwort, fid, frage, uid , name FROM ANTWORTFRAGEUMFRAGE");
			ResultSet rs = stmt.executeQuery();
			return mapAntwortFrageUmfragebogen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all antworten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
////////////////////////////////KANN MAN ES BESSER IMPLEMENTIEREN?
	
	public LinkedList<String> countAntworten(String tablevar)  {
		
		
		log.trace("countAntworten");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select count(*) as result from antworten");
			ResultSet rs = stmt.executeQuery();
			LinkedList<String> linkedlist = new LinkedList<String>();
			while (rs.next())		
			{
				int i = rs.getInt("result");
				String s = Integer.toString(i);
				linkedlist.add(s);
			}
						
			return linkedlist;			
		}
		catch(SQLException e)
		{
			String msg = "SQL error while counting antworten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	

	
	public int average()  {
		int avg = 0;
		log.trace("average");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("SELECT AVG(CAST(ANTWORT AS FLOAT)) as result1 FROM antworten where fid=9995");
			ResultSet rs = stmt.executeQuery();
			
			
			if(rs.next())
			    avg = rs.getInt(1);
			
			return avg;
						
		}
		catch(SQLException e)
		{
			String msg = "SQL error while processing average antworten. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	
	
	public Collection<DistinctValue> getDistinctVal(long fid)  {
		log.trace("getColor");
		try(Connection conn = getConnection())
		{
			
			PreparedStatement stmt = conn.prepareStatement("select antwort as cat, count(antwort) as quant from antworten where fid=? group by antwort");
			stmt.setLong(1, fid);
			ResultSet rs = stmt.executeQuery();
			
			return mapDistinctValue(rs);
						
					
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreving color. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}
	



	//////////////////////////////////////////
	/**
	 * Helper zum konvertieren der Resultsets in Antwort-Objekte. Siehe getByXXX Methoden.
	 * @author TOSE Avocado
	 * @throws SQLException 
	 *
	 */
	
	 
	private static Collection<Antwort> mapAntworten(ResultSet rs) throws SQLException 
	{
		LinkedList<Antwort> list = new LinkedList<Antwort>();
		while(rs.next())
		{
			Antwort i = new Antwort(rs.getLong("id"),rs.getString("antwort"),rs.getLong("fid"));
			list.add(i);
		}
		return list;
	}
	
	private static Collection<AntwortFrageUmfragebogen> mapAntwortFrageUmfragebogen(ResultSet rs) throws SQLException 
	{
		LinkedList<AntwortFrageUmfragebogen> list = new LinkedList<AntwortFrageUmfragebogen>();
		while(rs.next())
		{
			AntwortFrageUmfragebogen i = new AntwortFrageUmfragebogen(rs.getLong("id"),rs.getString("antwort"),rs.getLong("fid"),rs.getString("frage"),rs.getLong("uid"),rs.getString("name"));
			list.add(i);
		}
		return list;
	}
	
	private static Collection<DistinctValue> mapDistinctValue(ResultSet rs) throws SQLException 
	{
		LinkedList<DistinctValue> list = new LinkedList<DistinctValue>();
		while(rs.next())
		{
			DistinctValue i = new DistinctValue(rs.getString("cat"),rs.getInt("quant"));
			list.add(i);
		}
		return list;
	}
	
	

}
