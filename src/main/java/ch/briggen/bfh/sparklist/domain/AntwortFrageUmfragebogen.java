package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit alle attribute von Antwort, Frage un Umfragebogen -> für csv export
 * @author TOSE Avocado
 *
 */
public class AntwortFrageUmfragebogen {
	
	private long aid;
	private String antwort;
	private long fid;
	private String frage;
	private long uid;
	private String umfragebogen;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public AntwortFrageUmfragebogen()
	{
		
	}
	public AntwortFrageUmfragebogen(long aid, String antwort, long fid, String frage, long uid, String umfragebogen) {
		super();
		this.aid = aid;
		this.antwort = antwort;
		this.fid = fid;
		this.frage = frage;
		this.uid = uid;
		this.umfragebogen = umfragebogen;
	}
	public long getAid() {
		return aid;
	}

	public void setAid(long aid) {
		this.aid = aid;
	}

	public String getAntwort() {
		return antwort;
	}

	public void setAntwort(String antwort) {
		this.antwort = antwort;
	}

	public long getFid() {
		return fid;
	}

	public void setFid(long fid) {
		this.fid = fid;
	}

	public String getFrage() {
		return frage;
	}

	public void setFrage(String frage) {
		this.frage = frage;
	}

	public long getUid() {
		return uid;
	}

	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getUmfragebogen() {
		return umfragebogen;
	}

	public void setUmfragebogen(String umfragebogen) {
		this.umfragebogen = umfragebogen;
	}

	@Override
	public String toString() {
		return "AntwortFrageUmfragebogen [aid=" + aid + ", antwort=" + antwort + ", fid=" + fid + ", frage=" + frage
				+ ", uid=" + uid + ", umfragebogen=" + umfragebogen + "]";
	}

}
