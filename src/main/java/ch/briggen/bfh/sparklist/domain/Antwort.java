package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Antwort und fid (und einer eindeutigen Id)
 * @author TOSE Avocado
 *
 */
public class Antwort {
	
	private long id;
	private String antwort;
	private long fid;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Antwort()
	{
		
	}
	
	/**
	 * Konstruktor
	 *
	 * @param id
	 * @param antwort
	 * @param fid
	 */
	public Antwort(long id, String antwort, long fid) {
		super();
		this.id = id;
		this.antwort = antwort;
		this.fid = fid;
	}
	
	/**
	 * Getter und Setter
	 * 
	 */

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public String getAntwort() {
		return antwort;
	}

	public void setAntwort(String antwort) {
		this.antwort = antwort;
	}

	public long getFid() {
		return fid;
	}

	public void setFid(long fid) {
		this.fid = fid;
	}
	
	@Override
	public String toString() {
		return String.format("Frage:{id: %d; antwort: %s; fid: %d}", id, antwort, fid);
	}

}
