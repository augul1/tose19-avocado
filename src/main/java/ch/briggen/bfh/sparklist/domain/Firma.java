package ch.briggen.bfh.sparklist.domain;


/**
 * Einzelner Eintrag in der Liste mit Name und Ort (und einer eindeutigen Eid)
 * @author TOSE Avocado 2019
 *
 */
public class Firma {
	
	private long eid;
	private String name;
	private String ort;
	

public Firma () {
	
}
	
	public Firma(long eid, String name, String ort) {
	super();
	this.eid = eid;
	this.name = name;
	this.ort = ort;
}


	public long getEid() {
		return eid;
	}
	public void setEid(long eid) {
		this.eid = eid;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getOrt() {
		return ort;
	}
	public void setOrt(String ort) {
		this.ort = ort;
	}
	@Override
	public String toString() {
		return "Firma [eid=" + eid + ", name=" + name + ", ort=" + ort + "]";
	}
	
}



	