package ch.briggen.bfh.sparklist.domain;
import java.util.*; // wird für Collection gebraucht falls implementiert.

/**
 * Einzelner Eintrag in der Liste mit uid, type, frage (und einer eindeutigen Fid)
 * @author TOSE Avocado 2019
 *
 */
public class Frage {
	
	private long fid;
	private long uid;
	private String frage;
	private String type;

	/**
	 * Defaultkonstruktor für die Verwendung in einem Controller
	 */
	public Frage()
	{
		
	}
	
	public Frage(long fid, long uid, String frage, String type) {
		super();
		this.fid = fid;
		this.frage = frage;
		this.type = type;
		this.uid = uid;
	}
	

	/**
	 * getter und setter
	 */
	
	public String getType(){
		return type;
	}
	
	
	
	public void setType(String type) {
		this.type=type;
	}
	
	public long getFid() {
		return fid;
	}
	
	public void setFid(long fid) {
		this.fid = fid;
	}
	public long getUid() {
		return uid;
	}
	
	public void setUid(long uid) {
		this.uid = uid;
	}

	public String getFrage() {
		return frage;
	}

	public void setFrage(String frage) {
		this.frage = frage;
	}

	
	@Override
	public String toString() {
		return String.format("Frage:{fid: %d; uid: %d; frage: %s, type: %s}", fid, uid, frage, type);
	}
	

}
