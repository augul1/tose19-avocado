package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Firmen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Firma implementiert
 * @author TOSE Avocado 2019
 *
 */


public class FirmaRepository {
	
	private final Logger log = LoggerFactory.getLogger(FirmaRepository.class);
	

	/**
	 * Liefert alle items in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Firma> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select eid, name, ort from firma");
			ResultSet rs = stmt.executeQuery();
			return mapFirma(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all firma. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Firmen mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Firma> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select eid, name, ort from firma where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapFirma(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert das Firma mit der übergebenen Id
	 * @param eid eid des Firma
	 * @return Firma oder NULL
	 */
	public Firma getByEid(long eid) {
		log.trace("getByEid " + eid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select eid, name, ort from firma where eid=?");
			stmt.setLong(1, eid);
			ResultSet rs = stmt.executeQuery();
			return mapFirma(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by eid " + eid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert das übergebene firma in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Firma i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update firma set name=?, ort=? where eid=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getOrt());
			stmt.setLong(3, i.getEid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating firma " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht das Firma mit der angegebenen eid von der DB
	 * @param eid Firma ID
	 */
	public void delete(long eid) {
		log.trace("delete " + eid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from firma where eid=?");
			stmt.setLong(1, eid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing firma by eid " + eid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Firma in der DB. INSERT.
	 * @param i neu zu erstellendes Firma
	 * @return Liefert die von der DB generierte eid des neuen Firma zurück
	 */
	public long insert(Firma i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into firma (name, ort) values (?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getOrt());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long eid = key.getLong(1);
			return eid;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating firma " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Firma-Objekte. Siehe getByXXX Methoden.
	 * @author TOSE Avocado 2019
	 * @throws SQLException 
	 *
	 */
	private static Collection<Firma> mapFirma(ResultSet rs) throws SQLException 
	{
		LinkedList<Firma> list = new LinkedList<Firma>();
		while(rs.next())
		{
			Firma i = new Firma(rs.getLong("eid"),rs.getString("name"),rs.getString("ort"));
			list.add(i);
		}
		return list;
	}

}
