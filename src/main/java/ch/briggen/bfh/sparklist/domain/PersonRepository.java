package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Personen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Personen implementiert
 * @author TOSE Avocado 2019
 *
 */


public class PersonRepository {
	
	private final Logger log = LoggerFactory.getLogger(PersonRepository.class);
	

	/**
	 * Liefert alle Personen in der Datenbank
	 * @return Collection aller Personen
	 */
	public Collection<Person> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pid, name, vorname from person");
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all items. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}

	/**
	 * Liefert alle Personen mit dem angegebenen Namen
	 * @param name
	 * @return Collection mit dem Namen "name"
	 */
	public Collection<Person> getByName(String name) {
		log.trace("getByName " + name);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pid, name, vorname from person where name=?");
			stmt.setString(1, name);
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving person by name " + name;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
			
	}

	/**
	 * Liefert die Person mit der übergebenen pid
	 * @param pid von Person
	 * @return Person oder NULL
	 */
	public Person getById(long pid) {
		log.trace("getById " + pid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select pid, name, vorname from person where id=?");
			stmt.setLong(1, pid);
			ResultSet rs = stmt.executeQuery();
			return mapPerson(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving items by id " + pid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	/**
	 * Speichert die übergebende Person in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Person i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update person set name=?, vorname=? where pid=?");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.setLong(3, i.getPid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating person " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Person mit der angegebenen pid von der DB
	 * @param pid Person ID
	 */
	public void delete(long pid) {
		log.trace("delete " + pid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from person where pid=?");
			stmt.setLong(1, pid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing items by pid " + pid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert die angegebene Peron in der DB. INSERT.
	 * @param i neu zu erstellende Person
	 * @return Liefert die von der DB generierte pid der neuen Person zurück
	 */
	public long insert(Person i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into person (name,vorname) values (?,?)");
			stmt.setString(1, i.getName());
			stmt.setString(2, i.getVorname());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long pid = key.getLong(1);
			return pid;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating person " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	
	/**
	 * Helper zum konvertieren der Resultsets in Person-Objekte. Siehe getByXXX Methoden.
	 * @author TOSE Avocado 2019
	 * @throws SQLException 
	 *
	 */
	private static Collection<Person> mapPerson(ResultSet rs) throws SQLException 
	{
		LinkedList<Person> list = new LinkedList<Person>();
		while(rs.next())
		{
			Person i = new Person(rs.getLong("pid"),rs.getString("name"),rs.getString("vorname"));
			list.add(i);
		}
		return list;
	}

}
