package ch.briggen.bfh.sparklist.domain;

public class DistinctValue {
	
String cat;
long quant;

public DistinctValue(String cat, long quant) {
	this.cat = cat;
	this.quant = quant;

}

public String getCat() {
	return cat;
}

public long getQuant() {
	return quant;
}

public String toString() {
	return String.format("Item:{cat: %d; quant: %s}", cat, quant);
}
}
