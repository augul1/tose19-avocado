package ch.briggen.bfh.sparklist.domain;

import static ch.briggen.bfh.sparklist.domain.JdbcRepositoryHelper.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Collection;
import java.util.LinkedList;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * Repository für alle Fragen. 
 * Hier werden alle Funktionen für die DB-Operationen zu Fragen implementiert
 * @author TOSE Avocado 2019
 *
 */


public class FrageRepository {
	
	private final Logger log = LoggerFactory.getLogger(FrageRepository.class);
	

	/**
	 * Liefert alle antworten in der Datenbank
	 * @return Collection aller Items
	 */
	public Collection<Frage> getAll()  {
		log.trace("getAll");
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select fid, uid, frage, type from fragen");
			ResultSet rs = stmt.executeQuery();
			return mapFragen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving all fragen. ";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
	}


	/**
	 * Liefert die Frage mit der übergebenen fid
	 * @param fid fid der Frage
	 * @return Frage oder NULL
	 */
	public Frage getById(long fid) {
		log.trace("getById " + fid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select fid, uid, frage, type from fragen where fid=?");
			stmt.setLong(1, fid);
			ResultSet rs = stmt.executeQuery();
			return mapFragen(rs).iterator().next();		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by fid " + fid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	public String getTypeById(long fid) {
		log.trace("getTypeById " + fid);
		String type = null;
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select type as type from fragen where fid=?");
			stmt.setLong(1, fid);
			ResultSet rs = stmt.executeQuery();
			
			if(rs.next())
			    type = rs.getString("type");
			
			return type;
			
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by retrieving type";
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}
	
	
	/**
	 * Liefert die Frage mit der übergebenen Uid
	 * @param uid uid der Frage
	 * @return Frage oder NULL
	 */
	public Collection<Frage> getByUid(long uid) {
		log.trace("getById " + uid);
		
		//TODO: There is an issue with this repository method. Find and fix it!
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("select fid, uid, frage, type from fragen where uid=?");
			stmt.setLong(1, uid);
			ResultSet rs = stmt.executeQuery();
			return mapFragen(rs);		
		}
		catch(SQLException e)
		{
			String msg = "SQL error while retreiving antworten by uid " + uid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					
	}

	
	/**
	 * Speichert das übergebene Frage in der Datenbank. UPDATE.
	 * @param i
	 */
	public void save(Frage i) {
		log.trace("save " + i);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("update fragen set uid=?, frage=?, type=? where fid=?");
			stmt.setLong(1, i.getUid());
			stmt.setString(2, i.getFrage());
			stmt.setString(3, i.getType());
			stmt.setLong(4, i.getFid());
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating fragen " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}
		
	}

	/**
	 * Löscht die Frage mit der angegebenen fid von der DB
	 * @param fid Frage ID
	 */
	public void delete(long fid) {
		log.trace("delete " + fid);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("delete from fragen where fid=?");
			stmt.setLong(1, fid);
			stmt.executeUpdate();
		}
		catch(SQLException e)
		{
			String msg = "SQL error while deleteing fragen by fid " + fid;
			log.error(msg, e);
			throw new RepositoryException(msg);
		}
					

	}

	/**
	 * Speichert das angegebene Frage in der DB. INSERT.
	 * @param i neu zu erstellendes Frage
	 * @return Liefert die von der DB generierte fid des neuen Frage zurück
	 */
	public long insert(Frage i) {
		
		log.trace("insert " + i);

		//Integer id = jdbc.queryForObject("select IDENTITY();", Integer.class);
		
		try(Connection conn = getConnection())
		{
			PreparedStatement stmt = conn.prepareStatement("insert into fragen (uid,frage,type) values (?,?,?)");
			stmt.setLong(1, i.getUid());
			stmt.setString(2, i.getFrage());
			stmt.setString(3, i.getType());
			stmt.executeUpdate();
			ResultSet key = stmt.getGeneratedKeys();
			key.next();
			Long fid = key.getLong(1);
			return fid;
		}
		catch(SQLException e)
		{
			String msg = "SQL error while updating fragen " + i;
			log.error(msg , e);
			throw new RepositoryException(msg);
		}

	}
	

	/**
	 * Helper zum konvertieren der Resultsets in Frage-Objekte. Siehe getByXXX Methoden.
	 * @author TOSE Avocado 2019
	 * @throws SQLException 
	 *
	 */
	private static Collection<Frage> mapFragen(ResultSet rs) throws SQLException 
	{
		LinkedList<Frage> list = new LinkedList<Frage>();
		while(rs.next())
		{
			Frage i = new Frage(rs.getLong("fid"),rs.getLong("uid"),rs.getString("frage"),rs.getString("type"));
			list.add(i);
		}
		return list;
	}

}
