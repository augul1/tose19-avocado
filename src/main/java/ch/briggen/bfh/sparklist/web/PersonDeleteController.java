package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Personen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class PersonDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonDeleteController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	

	/**
	 * Löscht das Item mit der übergebenen pid in der Datenbank
	 * /person/delete&pid=987 löscht das Person mit der pid 987 aus der Datenbank
	 * 
	 * Hört auf GET /person/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String pid = request.queryParams("pid");
		log.trace("GET /person/delete mit pid " + pid);
		
		Long longPid = Long.parseLong(pid);
		personRepo.delete(longPid);
		response.redirect("/person");
		return null;
	}
}


