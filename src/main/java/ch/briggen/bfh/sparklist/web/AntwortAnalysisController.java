package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import ch.briggen.bfh.sparklist.domain.FrageRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für die Listenausgabe alle Fragen die zu eine ausgewählte Umfrage gehören.
 * 
 * 
 * @author TOSE Avocado 2019
 *
 */

public class AntwortAnalysisController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(AntwortAnalysisController.class);
		
	AntwortRepository antwort_repository = new AntwortRepository();
	FrageRepository frage_repository = new FrageRepository();

	
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		String uid = request.queryParams("uid");				// gepostete uid eingelesen und übergegeben
		log.trace("GET /zusammenfassung mit uid " + uid);
		Long longId = Long.parseLong(uid);
		
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("fragen", frage_repository.getByUid(longId)); //nur Fragen mit übergegebene Uid auswerten
		
		
		
		//model.put("avg",antwort_repository.average());
		
		
	

		return new ModelAndView(model, "antwortZusammenfassung");
	}
}


