package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Antwort;
import spark.Request;

class AntwortWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(AntwortWebHelper.class);
	
	public static Antwort[] antwortFromWeb(Request request) {
		String fid[] = request.queryParamsValues("antwortDetail.fid");
		String antwort[] = request.queryParamsValues("antwortDetail.antwort");
		String id[] = request.queryParamsValues("antwortDetail.id");
		String emoticon = request.queryParams("emoticon");
		String skala = request.queryParams("skala");
	if(antwort==null) {
		Antwort[] a = new Antwort[2];
		a[0]= new Antwort(9999,emoticon,9999);
		a[1]= new Antwort(9999,skala,9995);
		return a;
	}
	else {
		Antwort[] a = new Antwort[fid.length+2];
		for(int i=0; i<fid.length; i++) {
			a[i] = new Antwort(Long.parseLong(id[i]),antwort[i],Long.parseLong(fid[i]));
		}
		a[a.length-2] = new Antwort(9999,emoticon,9999); // ergebnis für hart kodierte fragen, aid wird in der laufzeit überschrieben
		a[a.length-1] = new Antwort(9999,skala,9995);
		return a;
	}
		
	}
	


}
