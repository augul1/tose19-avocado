package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Umfragebogen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class UmfragebogenDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UmfragebogenDeleteController.class);
		
	private UmfragebogenRepository umfragebogenRepo = new UmfragebogenRepository();
	

	/**
	 * Löscht das Umfragebogen mit der übergebenen uid in der Datenbank
	 * /umfragebogen/delete&uid=987 löscht das Umfragebogen mit der uid 987 aus der Datenbank
	 * 
	 * Hört auf GET /umfragebogen/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String uid = request.queryParams("uid");
		log.trace("GET /umfragebogen/delete mit uid " + uid);
		
		Long longId = Long.parseLong(uid);
		umfragebogenRepo.delete(longId);
		response.redirect("/umfragebogen");
		return null;
	}
}


