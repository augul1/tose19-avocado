package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import ch.briggen.bfh.sparklist.domain.FrageRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für Listenausgabe alle Antworten die zu eine selektionierte Frage gehören
 * 
 * @author TOSE Avocado 2019
 *
 */

public class AntwortAnalysisDetailController implements TemplateViewRoute  {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(AntwortAnalysisDetailController.class);
		
	AntwortRepository antwort_repository = new AntwortRepository();
	FrageRepository frage_repository = new FrageRepository();

	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		String fid = request.queryParams("fid");				// fid eingelesen und übergegeben (aus POST)
		log.trace("GET /antwortdetail mit fid " + fid);
		long loid = Long.parseLong(fid);
			
		HashMap<String, Object> model = new HashMap<String, Object>();
		model.put("antwort", antwort_repository.getByFid(loid)); //nur Antworten mit übergegebene fid auswerten

		return new ModelAndView(model, "antwortZusammenfassungDetail");
	}
}


