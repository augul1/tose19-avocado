package ch.briggen.bfh.sparklist.web;

// dise Klasse ist wegen der array zusammensetzung der AntwortWebHelper gemacht worden. Hilft nur beim Update eine einzelfrage


import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Antwort;
import spark.Request;

class AntwortWebHelperUpdate {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(AntwortWebHelper.class);
	
	public static Antwort antwortFromWeb(Request request)
	{
		return new Antwort(
				Long.parseLong(request.queryParams("antwortDetail.id")),
				request.queryParams("antwortDetail.antwort"),
				Long.parseLong(request.queryParams("antwortDetail.fid")));
	}

}
