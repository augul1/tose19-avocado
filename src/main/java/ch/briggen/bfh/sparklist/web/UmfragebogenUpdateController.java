package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Umfragebogen;
import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Umfragebogen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class UmfragebogenUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(UmfragebogenUpdateController.class);
		
	private UmfragebogenRepository umfragebogenRepo = new UmfragebogenRepository();
	


	/**
	 * Schreibt das geänderte umfragebogen zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/umfragebogen) mit der umfragebogen-uid als Parameter mit dem namen uid.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /umfragebogen/update
	 * 
	 * @return redirect nach /umfragebogen: via Browser wird /umfragebogen aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Umfragebogen umfragebogenDetail = UmfragebogenWebHelper.umfragebogenFromWeb(request);
		
		log.trace("POST /item/update mit umfragebogenDetail " + umfragebogenDetail);
		
		//Speichern des Umfragebogen in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /umfragebogen&uid=3 (wenn umfragebogenDetail.getUid == 3 war)
		umfragebogenRepo.save(umfragebogenDetail);
		response.redirect("/umfragebogen");
		return null;
	}
}


