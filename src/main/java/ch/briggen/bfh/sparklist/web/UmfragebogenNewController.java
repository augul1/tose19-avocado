package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Frage;
import ch.briggen.bfh.sparklist.domain.Umfragebogen;
import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen umfragebogen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class UmfragebogenNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(UmfragebogenNewController.class);
		
	private UmfragebogenRepository umfragebogenRepo = new UmfragebogenRepository();
	
	/**
	 * Erstellt ein neues umfragebogen in der DB. Die uid wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /umfragebogen&uid=99  wenn die uid 99 war.)
	 * 
	 * Hört auf POST /umfragebogen/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Umfragebogen umfragebogenDetail = UmfragebogenWebHelper.umfragebogenFromWeb(request);
		log.trace("POST /umfragebogen/new mit umfragebogenDetail " + umfragebogenDetail);
		
		//insert gibt die von der DB erstellte uid zurück.
		long id = umfragebogenRepo.insert(umfragebogenDetail);
		
		//Frage smiley = new Frage(0, id, "Wie war der Fragebogen", "SMILEY");
		//frageRepo.insert(frageDetail);

		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		response.redirect("/umfragebogen");
		return null;
	}
}


