package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Frage;
import ch.briggen.bfh.sparklist.domain.FrageRepository;
import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Fragen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class FrageEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(FrageEditController.class);
	
	
	
	private FrageRepository frageRepo = new FrageRepository();
	private UmfragebogenRepository umfragebogen_repository = new UmfragebogenRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eine Frage. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "frageDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String fidString = request.queryParams("fid");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == fidString)
		{
			log.trace("GET /frage für INSERT mit fid " + fidString);
			//der Submit-Button ruft /frage/new auf --> INSERT
			model.put("postAction", "/frage/new");
			model.put("frageDetail", new Frage());

		}
		else
		{
			log.trace("GET /frage für UPDATE mit fid " + fidString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/frage/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long fid = Long.parseLong(fidString);
			Frage i = frageRepo.getById(fid);
			model.put("frageDetail", i);
		}
		model.put("umfragebogen", umfragebogen_repository.getAll());
		model.put("fragen",frageRepo.getAll());
		
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "frageDetail");
	}
	
	
	
}


