package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Frage;
import spark.Request;

class FrageWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(FrageWebHelper.class);
	
	public static Frage frageFromWeb(Request request)
	{
		return new Frage(
				Long.parseLong(request.queryParams("frageDetail.fid")),
				Long.parseLong(request.queryParams("frageDetail.uid")),
				request.queryParams("frageDetail.frage"),
				request.queryParams("frageDetail.type"));
	}

}
