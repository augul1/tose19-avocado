package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.FrageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Fragen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author Marcel Briggen
 *
 */

public class FrageDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(FrageDeleteController.class);
		
	private FrageRepository frageRepo = new FrageRepository();

	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String fid = request.queryParams("fid");
		log.trace("GET /item/delete mit fid " + fid);
		
		Long longFid = Long.parseLong(fid);
		frageRepo.delete(longFid);
		response.redirect("/frage");
		return null;
	}
}
