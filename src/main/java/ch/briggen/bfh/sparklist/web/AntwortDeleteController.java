package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Antworten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class AntwortDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(AntwortDeleteController.class);
		
	private AntwortRepository antwortRepo = new AntwortRepository();
	

	/**
	 * Löscht das Antwort mit der übergebenen id in der Datenbank
	 * /antwort/delete&id=987 löscht das Antwort mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /antwort/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Antwortverwaltung Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String id = request.queryParams("id");
		log.trace("GET /antwort/delete mit id " + id);
		
		Long longId = Long.parseLong(id);
		antwortRepo.delete(longId);
		response.redirect("/antwortverwaltung");
		return null;
	}
}


