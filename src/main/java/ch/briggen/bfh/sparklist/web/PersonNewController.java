package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Person
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class PersonNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(PersonNewController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	
	/**
	 * Erstellt ein neues Person in der DB. Die pid wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /person&pid=99  wenn die pid 99 war.)
	 * 
	 * Hört auf POST /person/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		log.trace("POST /person/new mit personDetail " + personDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long pid = personRepo.insert(personDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		response.redirect("/person");
		return null;
	}
}


