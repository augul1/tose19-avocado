package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Personen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class PersonUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(PersonUpdateController.class);
		
	private PersonRepository personRepo = new PersonRepository();
	


	/**
	 * Schreibt das geänderte Person zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/person) mit der Person-pid als Parameter mit dem namen pid.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /person/update
	 * 
	 * @return redirect nach /person: via Browser wird /person aufgerufen, also editPerson weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Person personDetail = PersonWebHelper.personFromWeb(request);
		
		log.trace("POST /person/update mit personDetail " + personDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		personRepo.save(personDetail);
		response.redirect("/person");
		return null;
	}
}


