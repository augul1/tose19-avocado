package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Firma;
import ch.briggen.bfh.sparklist.domain.FirmaRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Firmen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class FirmaNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(FirmaNewController.class);
		
	private FirmaRepository firmaRepo = new FirmaRepository();
	
	/**
	 * Erstellt ein neues Firma in der DB. Die eid wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /firma&eid=99  wenn die eid 99 war.)
	 * 
	 * Hört auf POST /firma/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Firma firmaDetail = FirmaWebHelper.firmaFromWeb(request);
		log.trace("POST /firma/new mit firmaDetail " + firmaDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long eid = firmaRepo.insert(firmaDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		response.redirect("/firma");
		return null;
	}
}


