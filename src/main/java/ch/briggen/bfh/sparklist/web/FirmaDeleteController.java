package ch.briggen.bfh.sparklist.web;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.FirmaRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Firmen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class FirmaDeleteController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(FirmaDeleteController.class);
		
	private FirmaRepository firmaRepo = new FirmaRepository();
	

	/**
	 * Löscht das Item mit der übergebenen id in der Datenbank
	 * /item/delete&id=987 löscht das Item mit der Id 987 aus der Datenbank
	 * 
	 * Hört auf GET /item/delete (besser wäre POST)
	 * 
	 * @return Redirect zurück zur Liste
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String eid = request.queryParams("eid");
		log.trace("GET /firma/delete mit eid " + eid);
		
		Long longEid = Long.parseLong(eid);
		firmaRepo.delete(longEid);
		response.redirect("/firma");
		return null;
	}
}


