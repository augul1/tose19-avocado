package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Antwort;
import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Antworten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class AntwortNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(AntwortNewController.class);
		
	private AntwortRepository antwortRepo = new AntwortRepository();
	
	/**
	 * Erstellt ein neues Antwort in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /antwort&id=99  wenn die id 99 war.)
	 * 
	 * Hört auf POST /antwort/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Antwort[] antwortDetail = AntwortWebHelper.antwortFromWeb(request);
		log.trace("POST /antwort/new mit antwortDetail " + antwortDetail);
		
		//insert
		for(int i=0; i<antwortDetail.length; i++) {
		antwortRepo.insert(antwortDetail[i]);}
		
		//der redirect erfolgt dann auf /antwort
		response.redirect("/");
		return null;
	}
}


