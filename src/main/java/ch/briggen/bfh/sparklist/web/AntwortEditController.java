package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Antwort;
import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import ch.briggen.bfh.sparklist.domain.FrageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Antworten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class AntwortEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(AntwortEditController.class);
	
	
	
	private AntwortRepository antwortRepo = new AntwortRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Antwort. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /antwort/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /antwort/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		String idString = request.queryParams("id");
		HashMap<String, Object> model = new HashMap<String, Object>();
		FrageRepository frage_repository = new FrageRepository();
		
		//TODO: check if 0 or null
		if(null == idString)
		{	log.trace("GET /antwort für INSERT mit id " + idString);
			String uid = request.queryParams("uid");	
			log.trace("GET /antwort mit uid " + uid);
			Long longId = Long.parseLong(uid);
			model.put("fragen", frage_repository.getByUid(longId)); //nur Fragen mit übergegebene Uid
			
			//der Submit-Button ruft /item/new auf --> INSERT
			model.put("postAction", "/antwort/new");
			model.put("antwortDetail", new Antwort());
			//das Template antwortDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "antwortDetail");
			

		}
		else /* im frontend funktioniert das Antwort Update nicht! Problem: bei jeden update erzeugt AntwortWebHelper weiter die Smiles und Skala Antworten zusätlic
			*   PROBLEM NICHT BEHOBEN / es würde einfach auf die Funktionalität verzichtet!
			*   Lösung: neue template für update abrufen und neue WebHelper Klasse schreiben (...)
			*/
		{
			log.trace("GET /antwort für UPDATE mit id " + idString);
			//der Submit-Button ruft /antwort/update auf --> UPDATE
			model.put("postAction", "/antwort/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long id = Long.parseLong(idString);
			Antwort i = antwortRepo.getById(id);
			model.put("antwortDetail", i);
			//das Template antwortDetail verwenden und dann "anzeigen".
			return new ModelAndView(model, "antwortDetailTemplate");
		}
		

		
	}
	
	
	
}


