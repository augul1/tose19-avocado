package ch.briggen.bfh.sparklist.web;


import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * WWW-Controller
 * Liefert unter "/" die ganze Liste
 * 
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * 
 * @author M. Briggen
 *
 */
public class ListManagementRootController implements TemplateViewRoute {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(ListManagementRootController.class);

	
	/**
	 *Liefert die Liste als Root-Seite "/" zurück 
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		HashMap<String, Object> model = new HashMap<String, Object>();

		return new ModelAndView(model, "listTemplate");
	}
	

}
