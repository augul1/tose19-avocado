package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import ch.briggen.bfh.sparklist.domain.FrageRepository;

import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für die graphische darstellung der Antworten von eine bestimmte Frage
 * 
 * @author TOSE Avocado 2019
 *
 */

public class AntwortDiagramm implements TemplateViewRoute  {
	
	@SuppressWarnings("unused")
	private final Logger log = LoggerFactory.getLogger(AntwortDiagramm.class);
		
	
	AntwortRepository antwort_repository = new AntwortRepository();
	FrageRepository frage_repository = new FrageRepository();
	

	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		String modeltype=null;
		
		String fid = request.queryParams("fid");				// fid eingelesen und übergegeben
		log.trace("GET /AntwortDiagramm mit fid " + fid);
		long loid = Long.parseLong(fid);
		
		String type = request.queryParams("type");				// fid eingelesen und übergegeben
		log.trace("GET /AntwortDiagramm mit type " + type);
		
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		switch(type) {
		
		case "smiles":
		case "pie":
			
			
			
			model.put("DistinctValue",antwort_repository.getDistinctVal(loid));
			model.put("type",frage_repository.getTypeById(loid));
			
			modeltype = "antwortdiagramm";
			break;
			
		case "column":
			
			
			
			model.put("DistinctValue",antwort_repository.getDistinctVal(loid));
			model.put("type",frage_repository.getTypeById(loid));
			
			modeltype = "antwortdiagrammco";
			break;
			
		case "line":
			
			
			
			model.put("DistinctValue",antwort_repository.getDistinctVal(loid));
			model.put("type",frage_repository.getTypeById(loid));
			
			modeltype = "antwortdiagrammli";
			break;
			
		case "area":
			
			
			
			model.put("DistinctValue",antwort_repository.getDistinctVal(loid));
			model.put("type",frage_repository.getTypeById(loid));
			
			modeltype = "antwortdiagrammar";
			break;
			
		case "avg":
			
			
			
			//model.put("DistinctValue",antwort_repository.getDistinctVal(loid));
			model.put("avg",antwort_repository.average());
			
			modeltype = "antwortdiagrammavg";
			break;
			
			
			
		}
		
		
		
		return new ModelAndView(model, modeltype);
	}
}


