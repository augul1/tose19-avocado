package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Antwort;
import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Antworten
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class AntwortUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(AntwortUpdateController.class);
		
	private AntwortRepository antwortRepo = new AntwortRepository();
	


	/**
	 * Schreibt das geänderte Antwort zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/antwort) mit der Item-id als Parameter mit dem namen id.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /antwort/update
	 * 
	 * @return redirect nach /item: via Browser wird /item aufgerufen, also editItem weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Antwort antwortDetail = AntwortWebHelperUpdate.antwortFromWeb(request);
		
		log.trace("POST /antwort/update mit antwortDetail " + antwortDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		antwortRepo.save(antwortDetail);
		response.redirect("/antwortverwaltung");
		return null;
	}
}


