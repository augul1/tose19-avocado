package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Frage;
import ch.briggen.bfh.sparklist.domain.FrageRepository;
import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für die Startseite des Joymetry Verwaltung: keine Geschäftslogik!!
 * 
 * @author TOSE Avocado 2019
 *
 */

public class JoymetryAdminController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(JoymetryAdminController.class);
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		HashMap<String, Object> model = new HashMap<String, Object>();
		return new ModelAndView(model, "joymetryVerwaltung");
	}

}


