package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für die listenausgabe alle Umfragebogen für die Auswertung
 * 
 * @author TOSE Avocado 2019
 *
 */

public class AntwortAnalysisSelektionController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(AntwortAnalysisSelektionController.class);
	
	private UmfragebogenRepository umfragebogen_repository = new UmfragebogenRepository();
	
	
	/**
	 * 
	 * @return liste alle Umfragebogen in der DB
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		model.put("umfragebogen", umfragebogen_repository.getAll());
		
		return new ModelAndView(model, "antwortZusammenfassungSelektion");
	}
	
	
	
}


