package ch.briggen.bfh.sparklist.web;
import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.AntwortRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für istenausgabe alle Antworten in der DB, basis für csv export
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class AntwortVerwaltungController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(AntwortEditController.class);
	
	
	
	private AntwortRepository antwortRepo = new AntwortRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Antwort. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Antwort erstellt (Aufruf von /antwort/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Antwort mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /antwort
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "antwortVerwaltung" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		
		HashMap<String, Object> model = new HashMap<String, Object>();
		
		model.put("antworten", antwortRepo.getAll());
		model.put("antwortfrageumfragebogen", antwortRepo.getAllData());
		//das Template antwortVerwaltung verwenden und dann "anzeigen".
		return new ModelAndView(model, "antwortVerwaltung");
	}
}


