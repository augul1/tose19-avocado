package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Umfragebogen;
import ch.briggen.bfh.sparklist.domain.UmfragebogenRepository;
import ch.briggen.bfh.sparklist.domain.FirmaRepository;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Umfragebogen
 * 
 * @author TOSE Avocado 2019
 *
 */

public class UmfragebogenEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(FrageEditController.class);
	
	
	
	private UmfragebogenRepository umfragebogenRepo = new UmfragebogenRepository();
	private FirmaRepository firmaRepo = new FirmaRepository();
	private PersonRepository personRepo = new PersonRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eine Umfragebogen. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der uid Parameter 0 ist wird beim submitten des Formulars ein neues umfragebogen erstellt (Aufruf von /umfragebogen/new)
	 * Wenn der uid Parameter <> 0 ist wird beim submitten des Formulars das umfragebogen mit der übergebenen id upgedated (Aufruf /umfragebogen/save)
	 * Hört auf GET /umfragebogen
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "umfragebogenDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String uidString = request.queryParams("uid");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == uidString)
		{
			log.trace("GET /umfragebogen für INSERT mit uid " + uidString);
			//der Submit-Button ruft /umfragebogen/new auf --> INSERT
			model.put("postAction", "/umfragebogen/new");
			model.put("umfragebogenDetail", new Umfragebogen());
	 		
		}
		else
		{
			log.trace("GET /frage für UPDATE mit uid " + uidString);
			//der Submit-Button ruft /umfragebogen/update auf --> UPDATE
			model.put("postAction", "/umfragebogen/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. umfragebogenDetal muss dem im HTML-Template verwendeten Namen entsprechen 
			Long uid = Long.parseLong(uidString);
			Umfragebogen i = umfragebogenRepo.getByUid(uid);
			model.put("umfragebogenDetail", i);
	
		}
		model.put("firmen", firmaRepo.getAll());
		model.put("personen", personRepo.getAll());
		model.put("umfragebogen", umfragebogenRepo.getAll());
		//das Template umfragebogenDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "umfragebogenDetail");
	}
	
	
	
}




