package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import spark.Request;

class PersonWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(PersonWebHelper.class);
	
	public static Person personFromWeb(Request request)
	{
		return new Person(
				Long.parseLong(request.queryParams("personDetail.pid")),
				request.queryParams("name"),
				request.queryParams("vorname"));
	}

}
