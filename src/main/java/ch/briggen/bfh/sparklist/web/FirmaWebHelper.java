package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Firma;
import spark.Request;

class FirmaWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(FirmaWebHelper.class);
	
	public static Firma firmaFromWeb(Request request)
	{
		return new Firma(
				Long.parseLong(request.queryParams("firmaDetail.eid")),
				request.queryParams("firmaDetail.name"),
				request.queryParams("firmaDetail.ort"));
	}

}
