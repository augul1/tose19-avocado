package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Person;
import ch.briggen.bfh.sparklist.domain.PersonRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Personen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class PersonEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(PersonEditController.class);
	
	
	
	private PersonRepository personRepo = new PersonRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten einer Person. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der pid Parameter 0 ist wird beim submitten des Formulars ein neues Person erstellt (Aufruf von /person/new)
	 * Wenn der pid Parameter <> 0 ist wird beim submitten des Formulars das Person mit der übergebenen id upgedated (Aufruf /person/save)
	 * Hört auf GET /person
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "personDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		String pidString = request.queryParams("pid");
		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == pidString)
		{
			log.trace("GET /person für INSERT mit pid " + pidString);
			//der Submit-Button ruft /person/new auf --> INSERT
			model.put("postAction", "/person/new");
			model.put("personDetail", new Person());
			

		}
		else
		{
			log.trace("GET /person für UPDATE mit pid " + pidString);
			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/person/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "personDetail" hinzugefügt. personDetail muss dem im HTML-Template verwendeten Namen entsprechen 
			Long pid = Long.parseLong(pidString);
			Person i = personRepo.getById(pid);
			model.put("personDetail", i);
		}
		model.put("personen", personRepo.getAll());
		//das Template itemDetail verwenden und dann "anzeigen".
		return new ModelAndView(model, "personDetail");
	}
	
	
	
}


