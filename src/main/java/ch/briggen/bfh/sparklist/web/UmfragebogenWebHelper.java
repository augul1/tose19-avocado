package ch.briggen.bfh.sparklist.web;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Umfragebogen;
import spark.Request;

class UmfragebogenWebHelper {
	@SuppressWarnings("unused")
	private final static Logger log = LoggerFactory.getLogger(UmfragebogenWebHelper.class);
	
	public static Umfragebogen umfragebogenFromWeb(Request request)
	{
		return new Umfragebogen(
				Long.parseLong(request.queryParams("umfragebogenDetail.uid")),
				request.queryParams("umfragebogenDetail.name"),
				Long.parseLong(request.queryParams("umfragebogenDetail.eid")),
				Long.parseLong(request.queryParams("umfragebogenDetail.pid")));
	}

}
