package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Frage;
import ch.briggen.bfh.sparklist.domain.FrageRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute; 

/**
 * Controller für alle Operationen auf einzelnen Fragen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class FrageNewController implements TemplateViewRoute {
	
	private final Logger log = LoggerFactory.getLogger(FrageNewController.class);
		
	private FrageRepository frageRepo = new FrageRepository();
	
	/**
	 * Erstellt ein neues Frage in der DB. Die id wird von der Datenbank erstellt.
	 * Bei Erfolg wird wieder auf die Detailseite redirected (z.B.: /fragen&fid=99  wenn die fid 99 war.)
	 * 
	 * Hört auf POST /fragen/new
	 * 
	 * @return Redirect zurück zur Detailmaske
	 */	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Frage frageDetail = FrageWebHelper.frageFromWeb(request);
		log.trace("POST /frage/new mit frageDetail " + frageDetail);
		
		//insert gibt die von der DB erstellte id zurück.
		long fid = frageRepo.insert(frageDetail);
		
		//die neue Id wird dem Redirect als Parameter hinzugefügt
		//der redirect erfolgt dann auf /item?id=432932
		//response.redirect("/frage?fid="+fid);
		response.redirect("/frage");
		return null;
	}
}


