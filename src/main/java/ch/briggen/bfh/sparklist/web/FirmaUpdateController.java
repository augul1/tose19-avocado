package ch.briggen.bfh.sparklist.web;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Firma;
import ch.briggen.bfh.sparklist.domain.FirmaRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Firmen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class FirmaUpdateController implements TemplateViewRoute  {
	
	private final Logger log = LoggerFactory.getLogger(FirmaUpdateController.class);
		
	private FirmaRepository firmaRepo = new FirmaRepository();
	


	/**
	 * Schreibt das geänderte Firma zurück in die Datenbank
	 * Bei Erfolg erfolgt ein REDIRECT zurück auf die Detailseite (/firma) mit der Firma-eid als Parameter mit dem namen eid.
	 * Validierung: Im Fehlerfall wird eine durch Spring eine Fehlerseite generiert.
	 * 
	 * Hört auf POST /firma/update
	 * 
	 * @return redirect nach /firma: via Browser wird /firma aufgerufen, also edit Firma weiter oben und dann das Detailformular angezeigt.
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {
		Firma firmaDetail = FirmaWebHelper.firmaFromWeb(request);
		
		log.trace("POST /firma/update mit firmaDetail " + firmaDetail);
		
		//Speichern des Items in dann den Parameter für den Redirect abfüllen
		//der Redirect erfolgt dann z.B. auf /item&id=3 (wenn itemDetail.getId == 3 war)
		firmaRepo.save(firmaDetail);
		response.redirect("/firma");
		return null;
	}
}


