package ch.briggen.bfh.sparklist.web;

import java.util.HashMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.domain.Firma;
import ch.briggen.bfh.sparklist.domain.FirmaRepository;
import spark.ModelAndView;
import spark.Request;
import spark.Response;
import spark.TemplateViewRoute;

/**
 * Controller für alle Operationen auf einzelnen Firmen
 * !!! Diese Version verfügt bewusst über keine Validierung / Fehlerbehandlung !!!
 * @author TOSE Avocado 2019
 *
 */

public class FirmaEditController implements TemplateViewRoute{
	
	private final Logger log = LoggerFactory.getLogger(FirmaEditController.class);
	
	
	
	private FirmaRepository firmaRepo = new FirmaRepository();
	
	
	/**
	 * Requesthandler zum Bearbeiten eines Items. 
	 * Liefert das Formular (bzw. Template) zum bearbeiten der einzelnen Felder
	 * Wenn der id Parameter 0 ist wird beim submitten des Formulars ein neues Item erstellt (Aufruf von /item/new)
	 * Wenn der id Parameter <> 0 ist wird beim submitten des Formulars das Item mit der übergebenen id upgedated (Aufruf /item/save)
	 * Hört auf GET /item
	 * @return gibt den Namen des zu verwendenden Templates zurück. Immer "itemDetailTemplate" .
	 */
	
	@Override
	public ModelAndView handle(Request request, Response response) throws Exception {

		String eidString = request.queryParams("eid");

		HashMap<String, Object> model = new HashMap<String, Object>();
				
		//TODO: check if 0 or null
		if(null == eidString)
		{

			log.trace("GET /firma für INSERT mit eid " + eidString);
			//der Submit-Button ruft /firma/new auf --> INSERT

			model.put("postAction", "/firma/new");
			model.put("firmaDetail", new Firma());

		}
		else
		{

			log.trace("GET /firma für UPDATE mit eid " + eidString);

			//der Submit-Button ruft /item/update auf --> UPDATE
			model.put("postAction", "/firma/update");
			
			//damit die bereits in der Datenbank vorhandenen Werte im Formular gezeigt werden wird es geladen und dann
			//dem Modell unter dem Namen "itemDetail" hinzugefügt. itemDetal muss dem im HTML-Template verwendeten Namen entsprechen 

			Long eid = Long.parseLong(eidString);
			Firma i = firmaRepo.getByEid(eid);
			model.put("firmaDetail", i);

		}
		model.put("firmen", firmaRepo.getAll());
		//das Template itemDetail verwenden und dann "anzeigen".

		return new ModelAndView(model, "firmaDetail");

	}
	
	
	
}


