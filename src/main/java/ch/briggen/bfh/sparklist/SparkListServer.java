package ch.briggen.bfh.sparklist;

import static spark.Spark.get;
import static spark.Spark.post;

import java.util.Map.Entry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ch.briggen.bfh.sparklist.web.ListManagementRootController;
import ch.briggen.sparkbase.H2SparkApp;
import ch.briggen.sparkbase.UTF8ThymeleafTemplateEngine;

import ch.briggen.bfh.sparklist.web.ItemDeleteController;
import ch.briggen.bfh.sparklist.web.ItemEditController;
import ch.briggen.bfh.sparklist.web.ItemNewController;
import ch.briggen.bfh.sparklist.web.ItemUpdateController;

import ch.briggen.bfh.sparklist.web.AntwortDeleteController;
import ch.briggen.bfh.sparklist.web.AntwortDiagramm;
import ch.briggen.bfh.sparklist.web.AntwortEditController;
import ch.briggen.bfh.sparklist.web.AntwortNewController;
import ch.briggen.bfh.sparklist.web.AntwortUpdateController;
import ch.briggen.bfh.sparklist.web.AntwortAnalysisController;
import ch.briggen.bfh.sparklist.web.AntwortAnalysisDetailController;
import ch.briggen.bfh.sparklist.web.AntwortAnalysisSelektionController;
import ch.briggen.bfh.sparklist.web.AntwortVerwaltungController;

import ch.briggen.bfh.sparklist.web.FrageSelektionController;
import ch.briggen.bfh.sparklist.web.FrageDeleteController;
import ch.briggen.bfh.sparklist.web.FrageEditController;
import ch.briggen.bfh.sparklist.web.FrageNewController;
import ch.briggen.bfh.sparklist.web.FrageUpdateController;

import ch.briggen.bfh.sparklist.web.UmfragebogenEditController;
import ch.briggen.bfh.sparklist.web.UmfragebogenNewController;
import ch.briggen.bfh.sparklist.web.UmfragebogenDeleteController;
import ch.briggen.bfh.sparklist.web.UmfragebogenUpdateController;

import ch.briggen.bfh.sparklist.web.JoymetryAdminController;

import ch.briggen.bfh.sparklist.web.FirmaDeleteController;
import ch.briggen.bfh.sparklist.web.FirmaEditController;
import ch.briggen.bfh.sparklist.web.FirmaNewController;
import ch.briggen.bfh.sparklist.web.FirmaUpdateController;

import ch.briggen.bfh.sparklist.web.PersonDeleteController;
import ch.briggen.bfh.sparklist.web.PersonEditController;
import ch.briggen.bfh.sparklist.web.PersonNewController;
import ch.briggen.bfh.sparklist.web.PersonUpdateController;


public class SparkListServer extends H2SparkApp {

	final static Logger log = LoggerFactory.getLogger(SparkListServer.class);

	public static void main(String[] args) {
		
		for(Entry<Object, Object> property: System.getProperties().entrySet()) {
			log.debug(String.format("Property %s : %s", property.getKey(),property.getValue()));
		}

		SparkListServer server = new SparkListServer();
		server.configure();
		server.run();
	}

	@Override
	protected void doConfigureHttpHandlers() {
	
		get("/", new ListManagementRootController(), new UTF8ThymeleafTemplateEngine());
		
		get("/item", new ItemEditController(), new UTF8ThymeleafTemplateEngine());
		post("/item/update", new ItemUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/item/delete", new ItemDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/item/new", new ItemNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/antwort", new AntwortEditController(), new UTF8ThymeleafTemplateEngine());
		post("/antwort/new", new AntwortNewController(), new UTF8ThymeleafTemplateEngine());
		post("/antwort/update", new AntwortUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/antwort/delete", new AntwortDeleteController(), new UTF8ThymeleafTemplateEngine());
		
		//  listenausgabe alle fragen die zur selektionierte Umfrage gehören
		get("/zusammenfassung", new AntwortAnalysisController(), new UTF8ThymeleafTemplateEngine());
		
		// listenausgabe alle Antworten zur selektionierte Frage
		get("/antwortdetail", new AntwortAnalysisDetailController(), new UTF8ThymeleafTemplateEngine());
		
		// graphische darstellung der Antworten von eine bestimmte Frage
		get("/antwortdiagramm", new AntwortDiagramm(), new UTF8ThymeleafTemplateEngine());
		get("/antwortdiagrammco", new AntwortDiagramm(), new UTF8ThymeleafTemplateEngine());
		get("/antwortdiagrammcli", new AntwortDiagramm(), new UTF8ThymeleafTemplateEngine());
		get("/antwortdiagrammcar", new AntwortDiagramm(), new UTF8ThymeleafTemplateEngine());
		get("/antwortdiagrammavg", new AntwortDiagramm(), new UTF8ThymeleafTemplateEngine());

		// selektion eine Umfrage für die Auswertung
		get("/zusammenfassungselektion", new AntwortAnalysisSelektionController(), new UTF8ThymeleafTemplateEngine());
		
		// listenausgabe alle Antworten in der DB, basis für csv export
		get("/antwortverwaltung", new AntwortVerwaltungController(), new UTF8ThymeleafTemplateEngine());
		
		// Ausgabe alle Umfragebogen in der DB 
		get("/frageselektion", new FrageSelektionController(), new UTF8ThymeleafTemplateEngine());
		get("/frage", new FrageEditController(), new UTF8ThymeleafTemplateEngine());
		post("/frage/new", new FrageNewController(), new UTF8ThymeleafTemplateEngine());
		post("/frage/update", new FrageUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/frage/delete", new FrageDeleteController(), new UTF8ThymeleafTemplateEngine());
		
		get("/umfragebogen", new UmfragebogenEditController(), new UTF8ThymeleafTemplateEngine());
		post("/umfragebogen/new", new UmfragebogenNewController(), new UTF8ThymeleafTemplateEngine());
		post("/umfragebogen/update", new UmfragebogenUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/umfragebogen/delete", new UmfragebogenDeleteController(), new UTF8ThymeleafTemplateEngine());
		
		get("/joymetryadmin", new JoymetryAdminController(), new UTF8ThymeleafTemplateEngine());
		
		get("/firma", new FirmaEditController(), new UTF8ThymeleafTemplateEngine());
		post("/firma/update", new FirmaUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/firma/delete", new FirmaDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/firma/new", new FirmaNewController(), new UTF8ThymeleafTemplateEngine());
		
		get("/person", new PersonEditController(), new UTF8ThymeleafTemplateEngine());
		post("/person/update", new PersonUpdateController(), new UTF8ThymeleafTemplateEngine());
		get("/person/delete", new PersonDeleteController(), new UTF8ThymeleafTemplateEngine());
		post("/person/new", new PersonNewController(), new UTF8ThymeleafTemplateEngine());
		
	}

}
