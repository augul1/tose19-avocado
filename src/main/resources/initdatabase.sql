drop table IF EXISTS items;
drop view if exists antwortfrageumfrage;
drop table IF EXISTS antworten;
drop table IF EXISTS fragen;
drop table IF EXISTS umfragebogen;
drop table IF EXISTS firma;
drop table IF EXISTS person;

create table if not exists items (id long identity, name varchar(200), quantity decimal);
create table if not exists person (pid long identity, name varchar(200), vorname varchar(200), primary key (pid));
create table if not exists firma (eid long identity, name varchar(200), ort varchar(200), primary key (eid));
create table if not exists umfragebogen (uid long identity, name varchar(200), eid long, pid long, primary key (uid), foreign key (eid) references firma(eid) on delete set null, foreign key (pid) references person(pid) on delete set null);
create table if not exists fragen (fid long identity, uid long, frage varchar(200), type varchar(200), primary key (fid), foreign key (uid) references umfragebogen(uid) on delete cascade );
create table if not exists antworten (id long identity, antwort varchar(200), fid long, foreign key (fid) references fragen(fid) on delete cascade);
create view antwortfrageumfrage as SELECT Antworten.id, Antworten.antwort, Fragen.fid, Fragen.frage, Umfragebogen.uid, Umfragebogen.name FROM Antworten INNER JOIN Fragen INNER JOIN Umfragebogen ON  Fragen.fid=Antworten.fid AND Umfragebogen.uid=Fragen.uid;

insert into person (pid,name,vorname) values (9999,'John','Doe');
insert into person (pid,name,vorname) values (9998,'Nikita','Json');
insert into firma (eid, name, ort) values (9999,'Dummy','Place');
insert into firma (eid,name,ort) values (9998,'Wernli','Milano');
insert into umfragebogen (uid,name,eid,pid) values (9999, 'StandartUmfrage', 9999, 9999);
insert into umfragebogen (uid,name,eid,pid) values (9998, 'Knusprigkeitsanalyse', 9998, 9998);

insert into fragen (fid,uid,frage,type) values (9995, 9999,'Zufriedenheitsskala?','avg');
insert into fragen (fid,uid,frage,type) values (9999, 9999,'Sind Sie happy?','smiles');
insert into fragen (fid,uid,frage,type) values (9998, 9998,'Wie viele Japonais essen Sie pro Woche?','column');
insert into fragen (fid,uid,frage,type) values (9997, 9998,'Was gibt es in der füllung der "Wiener Waffel Exotic"?','column');
insert into fragen (fid,uid,frage,type) values (9996, 9998,'WernliCH importiert Orangen aus Sizilien, ja oder nein?','pie');

insert into antworten (id,antwort,fid) values (9999,'1',9998);
insert into antworten (id,antwort,fid) values (9998,'3',9998);
insert into antworten (id,antwort,fid) values (9997,'4',9998);
insert into antworten (id,antwort,fid) values (9996,'18',9998);
insert into antworten (id,antwort,fid) values (9995,'75',9998);
insert into antworten (id,antwort,fid) values (9994,'45',9998);

insert into antworten (id,antwort,fid) values (9993,'nix',9997);
insert into antworten (id,antwort,fid) values (9992,'meat ball',9997);
insert into antworten (id,antwort,fid) values (9991,'schoggi',9997);
insert into antworten (id,antwort,fid) values (9990,'schoggi',9997);
insert into antworten (id,antwort,fid) values (9989,'honig',9997);
insert into antworten (id,antwort,fid) values (9988,'nix',9997);

insert into antworten (id,antwort,fid) values (9987,'ja',9996);
insert into antworten (id,antwort,fid) values (9986,'ja',9996);
insert into antworten (id,antwort,fid) values (9985,'nein',9996);
insert into antworten (id,antwort,fid) values (9984,'nein',9996);
insert into antworten (id,antwort,fid) values (9983,'nein',9996);
insert into antworten (id,antwort,fid) values (9982,'ja',9996);

insert into antworten (id,antwort,fid) values (9981,'happy',9999);
insert into antworten (id,antwort,fid) values (9980,'happy',9999);
insert into antworten (id,antwort,fid) values (9979,'happy',9999);
insert into antworten (id,antwort,fid) values (9978,'unhappy',9999);
insert into antworten (id,antwort,fid) values (9977,'happy',9999);
insert into antworten (id,antwort,fid) values (9976,'happy',9999);

insert into antworten (id,antwort,fid) values (9975,'56',9995);
insert into antworten (id,antwort,fid) values (9974,'88',9995);
insert into antworten (id,antwort,fid) values (9973,'93',9995);
insert into antworten (id,antwort,fid) values (9972,'59',9995);
insert into antworten (id,antwort,fid) values (9971,'100',9995);
insert into antworten (id,antwort,fid) values (9970,'90',9995);

insert into person (pid,name,vorname) values (9997,'Dräyer','Thomas');
insert into firma (eid, name, ort) values (9997,'BfH','Bern');
insert into umfragebogen (uid,name,eid,pid) values (9997, 'Fazit Team Avocado', 9997, 9997);

insert into fragen (fid,uid,frage,type) values (9994, 9997,'Wie schätzen Sie den Lerneffekt dieser Arbeit?','avg');
insert into fragen (fid,uid,frage,type) values (9993, 9997,'Wie fanden Sie den Schwierigkeitsgrad des Moduls TOSE?','column');
insert into fragen (fid,uid,frage,type) values (9992, 9997,'Beschreiben Sie mit einem Stichwort die Zusammenarbeit im Avocado Team.','pie');

insert into antworten (id,antwort,fid) values (9969,'100',9994);
insert into antworten (id,antwort,fid) values (9968,'70',9994);
insert into antworten (id,antwort,fid) values (9967,'90',9994);
insert into antworten (id,antwort,fid) values (9966,'90',9994);
insert into antworten (id,antwort,fid) values (9965,'90',9994);

insert into antworten (id,antwort,fid) values (9964,'mittel',9993);
insert into antworten (id,antwort,fid) values (9963,'schwierig',9993);
insert into antworten (id,antwort,fid) values (9962,'schwierig',9993);
insert into antworten (id,antwort,fid) values (9961,'mittel',9993);
insert into antworten (id,antwort,fid) values (9960,'schwierig',9993);

insert into antworten (id,antwort,fid) values (9959,'glänzend',9992);
insert into antworten (id,antwort,fid) values (9958,'Kommunikation',9992);
insert into antworten (id,antwort,fid) values (9957,'wertschätzend',9992);
insert into antworten (id,antwort,fid) values (9956,'Kaffee-reich',9992);
insert into antworten (id,antwort,fid) values (9955,'Huere geil',9992);